import { arrTheme } from "../../Theme/ThemeManager";
import { ToDoListDarkTheme } from "../../Theme/ToDoListDarkTheme";

import {
  add_task,
  done_task,
  delete_task,
  edit_task,
  update_task,
} from "../types/ToDoListTypes";

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    { id: "task-1", taskName: "task 1", done: true },
    { id: "task-2", taskName: "task 2", done: false },
    { id: "task-3", taskName: "task 3", done: true },
    { id: "task-4", taskName: "task 4", done: false },
  ],
  taskEdit: { id: "task-1", taskName: "task 1", done: false },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case add_task: {
      if (action.newTask.taskName.trim() == "") {
        alert("hãy nhập tên công việc");
        return { ...state };
      }
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex(
        (task) => task.taskName === action.newTask.taskName
      );
      if (index !== -1) {
        alert("tên công việc đã tồn tại");
        return { ...state };
      }
      taskListUpdate.push(action.newTask);
      state.taskList = taskListUpdate;
      return { ...state };
    }
    case "change_theme": {
      let theme = arrTheme.find((theme) => theme.id == action.themeId);
      if (theme) {
        state.themeToDoList = { ...theme.theme };
      }
      return { ...state };
    }
    case done_task: {
      let TaskListUpdate = [...state.taskList];
      state.taskList = TaskListUpdate;
      let index = TaskListUpdate.findIndex((task) => task.id === action.taskId);
      if (index !== -1) {
        TaskListUpdate[index].done = true;
      }

      return { ...state, taskList: TaskListUpdate };
    }
    case delete_task: {
      let TaskListUpdate = [...state.taskList];
      TaskListUpdate = TaskListUpdate.filter(
        (task) => task.id !== action.taskId
      );
      return { ...state, taskList: TaskListUpdate };
    }

    case edit_task: {
      return { ...state, taskEdit: action.task };
    }
    
    case update_task: {
      state.taskEdit = { ...state.taskEdit, taskName: action.taskName };

      let taskListUpdate = [...state.taskList];

     let index = taskListUpdate.findIndex(task => task.id === state.taskEdit.id);
     console.log('index: ', index);

      if (index !== -1) {
        taskListUpdate[index] = state.taskEdit;
      }
      state.taskList = taskListUpdate;
      return { ...state };
    }
    default:
      return { ...state };
  }
};
